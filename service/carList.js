//controller

const res = require('express/lib/response');
const { carsData } = require('../models')

const showMainPage = async (req, res) => {
    const value = await carsData.findAll();
    res.render("main", {
        cars: value

    });
    // console.log(value)
    // res.redirect("/");

}

const createData = (req, res) => {
    //console.log(req.body.carInput)
    carsData.create({
        carName: req.body.carInput,
        price: req.body.priceInput,
        sizeProduct: req.body.sizeInput,
        carImg: req.body.imgInput,
    })
    res.redirect("/");
}

const showAddPage = (req, res) => {
    res.render("page/content");

}

const deleteContent = (req, res) => {

    carsData.destroy({
        where: {
            id: req.params.id,
        },
    }).then(() => console.log(`Data ${req.params.id} | deleted`));
    res.redirect("/")
}

const updatePage = async (req, res) => {
    // res.render("page/updt");
    const car = await carsData.findOne({

        where: { id: req.params.id }
    })

    // .then(carsData => console.log(carsData));
    // let newcar = JSON.stringify(car, null, 2);
    // newcar = JSON.parse(newcar);
    res.render("page/updt", {
        cars: car,
    })
    console.log(req.params.id);
    // console.log(newcar);


}

const updateContent = (req, res) => {
    let size = "";
    let rb = req.body;
    if (rb.sizeInput == "Choose Size") {
        size = rb.sizeText;
    } else {
        size = rb.sizeInput
    }

    console.log(req.body);

    const updated = {
        where: {
            id: req.body.id,

        },
    };

    carsData.update({
        carName: req.body.carInput,
        price: req.body.priceInput,
        sizeProduct: size,
        carImg: req.body.imgInput,
    }, updated).catch((err) => {
        console.error("Fail!");
    });
    res.redirect("/")
};

const filterData = async (req, res) => {
    const value = await carsData.findAll();
    const newValue = value.filter((row) => row.sizeProduct == req.params.size);

    // console.log(req.params.size)
    res.render("main", {
        cars: newValue,
    },
    )
    console.log(value)
    console.log(newValue)
};




module.exports = { showMainPage, createData, showAddPage, deleteContent, updatePage, updateContent, filterData }

