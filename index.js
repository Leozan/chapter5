//import..
const express = require('express');
const app = express();
const bp = require('body-parser')
//const jsonParser = bodyParser.json()

// const urlencodedParser = bodyParser.urlencoded({ extended: false });
// app.use(bp.json());

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

//router
const carWeb = require("./service/carList")

app.use(express.json());
//app.use(expLayout());
app.set("view engine", "ejs");
app.use(express.static("public"));


app.get("/", carWeb.showMainPage);
app.get("/add", carWeb.showAddPage);
app.get("/update/:id", carWeb.updatePage);
app.post("/update", carWeb.updateContent);
app.get("/delete/:id", carWeb.deleteContent);
app.post("/create", carWeb.createData);
app.get("/filter/:size", carWeb.filterData);



//server kickstart..
app.listen(5000, function () {
    console.log('Server on http://localhost:5000')
})

//notes untuk filter
//setiap tombol kasih parameter,
//di main ejs dikasi di button href /filter/small
// app.get("/filter/:size",)

//di controller ambil semua data pakai findALl()
//didalam findAll dengan value size: small/medium
//dimasukin kedalam respon
//buat function baru, trus dimasukin kedalam render
//const filterdata = async (size) => {
//     const data = await cars.findAll();
//     const newcars = data.filter((row) => row.size == size);
//     return newcars;
//   };