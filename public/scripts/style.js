const bodyside = document.body;
const sidebar = document.getElementById("sidebar");
const btnSide = document.getElementById("btnSideBar");
const btnAdd = document.getElementById("btnAdd");
const mainContent = document.getElementById("contentBodyMain");
const filterContent = document.getElementById("contentBody");

function openSideBar() {
    bodyside.style.marginLeft = "200px";
    bodyside.style.transition = "1000ms";
    sidebar.style.display = "flex";
    sidebar.style.animation = "myAnim 1500ms ease 0s 1 normal forwards";
    btnSide.style.display = "none";
    btnSide.style.transition = "300ms";
};

function closeSideBar() {
    sidebar.style.display = "none";
    bodyside.style.marginLeft = "0px";
    bodyside.style.transition = "1000ms";
    sidebar.style.transition = "1000ms";
    btnSide.style.display = "block";
    btnSide.style.transition = "300ms";
}

function changePage() {
    mainContent.style.display = "none";
    filterContent.style.display = "block";
    mainContent.style.transition = "500ms";
    filterContent.style.transition = "500ms";


}

function cancel() {
    filterContent.style.display = "none";
    mainContent.style.display = "block";
}

function submitData() {
    const getInput = () => {
        let carName = document.getElementById("carName").value;
        console.log(carName)
    }
    getInput();
    filterContent.style.display = "none";
    mainContent.style.display = "block";
}