'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class carsData extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  carsData.init({
    carName: DataTypes.STRING,
    price: DataTypes.STRING,
    sizeProduct: DataTypes.STRING,
    carImg: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'carsData',
  });
  return carsData;
};